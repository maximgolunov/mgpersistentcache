Pod::Spec.new do |s|
  s.name          = "MGPersistentCache"
  s.version       = "0.0.3"
  s.summary       = "A file cache that keeps in sync with remote files."

  s.description   = <<-DESC
                   A file cache that keeps syncing up with remote counterparts on demand.		
                   DESC

  s.homepage      = "https://gitlab.com/maximgolunov/mgpersistentcache"
  s.license       = "MIT"
  s.author        = { "Maxim Golunov" => "maxim.golunov@gmail.com" }
  s.platform      = :ios, '6.1'
  s.source        = { :git => "https://gitlab.com/maximgolunov/mgpersistentcache.git", :tag => s.version.to_s }
  s.source_files  = "MGPersistentCache/*.{h,m}"
  s.requires_arc  = true
  s.dependency "AFNetworking", "~> 2.5"
end
