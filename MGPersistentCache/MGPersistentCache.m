//
//  PersistentCache.m
//  
//
//  Created by Maksim Golunov on 4/4/12.
//  Copyright (c) 2012. All rights reserved.
//

#import "MGPersistentCache.h"
#import <AFNetworking/AFNetworking.h>

NSString* const kMGPersistentCacheErrorDomain = @"MGPersistentCacheErrorDomain";
static const unsigned long long kSizeThreshold = 64 * 1024;

@interface MGPersistentCache()

@property (nonatomic, readonly) NSMutableSet* downloadsInProgress;

@end

@implementation MGPersistentCache

@synthesize downloadsInProgress = _downloadsInProgress;

+ (instancetype)sharedCache
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+ (NSString*)cacheDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *cacheDirectoryName = [documentsDirectory stringByAppendingPathComponent:@"Downloads"];
    
    NSError* error = nil;
    if (![[NSFileManager defaultManager] fileExistsAtPath:cacheDirectoryName]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:cacheDirectoryName withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    NSAssert(error == nil, @"Error creating download cache directory: %@", [error localizedDescription]);
    
    return cacheDirectoryName;
}

+ (NSString*)fileNameForURL:(NSURL*)url
{
    return [NSString stringWithFormat:@"%lu_%@", (unsigned long)[url hash], [url lastPathComponent]];
}

+ (NSString*)localPathForURL:(NSURL*)url
{
    NSString *path = [[[self class] cacheDirectory] stringByAppendingPathComponent:[[self class] fileNameForURL:url]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return path;
    }
    
    return nil;
}

- (id)init
{
    if (self = [super init]) {
        _downloadsInProgress = [NSMutableSet new];
    }
    
    return self;
}


- (void)localPathForURL:(NSURL *)url completion:(void(^)(NSString* path, NSError* error))completion progress:(void(^)(NSUInteger bytesRead, long long totalBytes, long long totalBytesExpected))progress
{
    NSString *path = [[[self class] cacheDirectory] stringByAppendingPathComponent:[[self class] fileNameForURL:url]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        if (![_downloadsInProgress containsObject:url]) {
            [self downloadFileAtURL:url toPath:path completion:completion progress:progress];
        } else {
            NSError* error = [NSError errorWithDomain:kMGPersistentCacheErrorDomain code:MGPersistentCacheErrorDownloadInProgress userInfo:nil];
            completion(nil, error);
        }
    } else {
        // give out stored file so far
        completion(path, nil);
        
        // check remote and local file size
        NSError* error = nil;
        NSDictionary* fAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:&error];
        
        if (error) {
            NSLog(@"Error obtaining file attributes at path %@: %@", path, error);
            completion(path, error);
        } else {
            unsigned long long contentLengthLocal = [fAttributes fileSize];
            if (contentLengthLocal > kSizeThreshold) {
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                [request setHTTPMethod:@"HEAD"];
                            
                AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                
                [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                    unsigned long long contentLengthRemote = [[[[operation response] allHeaderFields] objectForKey:@"Content-Length"] intValue];
                    
                    NSLog(@"Remote file size: %llu, local: %llu", contentLengthRemote, contentLengthLocal);
                    if (contentLengthRemote > 0 && contentLengthRemote != contentLengthLocal) {
                        [self downloadFileAtURL:url toPath:path completion:completion progress:progress];
                    }/* else {
                        completion(path, nil);
                    }*/
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"Error checking remote file size: %@, ignoring", error);
                }];
                
                [operation start];
            }
        }
    }
}

- (void)localPathsForURLs:(NSArray *)urls itemCompletion:(void (^)(NSURL * url, NSString * path, NSError * error))itemCompletion itemProgress:(void (^)(NSURL* url, NSUInteger bytesRead, long long totalBytes, long long totalBytesExpected))itemProgress overallCompletion:(void (^)(NSArray * paths, NSError * error))overallCompletion
{
    __block NSError *error = nil;
    __block NSMutableArray* paths = [NSMutableArray arrayWithCapacity:[urls count]];
    
    dispatch_group_t group = dispatch_group_create();
    
    for (NSURL* url in urls) {
        dispatch_group_enter(group);
        [self localPathForURL:url completion:^(NSString *path, NSError *_error) {
            if (_error) {
                error = _error;
            }
            
            if (itemCompletion) {
                itemCompletion(url, path, _error);
            }
            
            if (path) {
                [paths addObject:path];
            }
            
            dispatch_group_leave(group);
        } progress:^(NSUInteger bytesRead, long long totalBytes, long long totalBytesExpected) {
            if (itemProgress) {
                itemProgress(url, bytesRead, totalBytes, totalBytesExpected);
            }
        }];
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        if (overallCompletion) {
            overallCompletion(paths, error);
        }
    });
}

- (void)downloadFileAtURL:(NSURL*)url toPath:(NSString*)path completion:(void(^)(NSString* path, NSError* error))completion progress:(void(^)(NSUInteger bytesRead, long long totalBytes, long long totalBytesExpected))progress
{
    NSLog(@"Downloading file at URL: %@ to %@", [url absoluteString], path);
    
    [_downloadsInProgress addObject:url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
        
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    NSString* pathPart = [path stringByAppendingString:@".part"];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:pathPart append:NO];
    [operation setDownloadProgressBlock:progress];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* error = nil;
        if ([[NSFileManager defaultManager] fileExistsAtPath:pathPart]) {
            if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
                if (![[NSFileManager defaultManager] removeItemAtPath:path error:&error]) {
                    NSLog(@"Failed to remove existing file at path [%@]: %@", path, error);
                    completion(path, error);
                    return;
                }
            }
            [[NSFileManager defaultManager] moveItemAtPath:pathPart toPath:path error:&error];
            NSLog(@"Downloaded and moved file to %@: %@", path, error ? error : @"success");
        }
        [_downloadsInProgress removeObject:url];
        completion(path, error);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error downloading file: %@", error);
        [_downloadsInProgress removeObject:url];
        completion(nil, error);
    }];
    
    [operation start];
}

+ (void)clearCacheForURL:(NSString *)remoteURL
{
    NSString *path = [[[self class] cacheDirectory] stringByAppendingPathComponent:[[self class] fileNameForURL:[NSURL URLWithString:remoteURL]]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
}

+ (void)clearCache
{
    [[NSFileManager defaultManager] removeItemAtPath:[[self class] cacheDirectory] error:nil];
}


@end
