//
//  PersistentCache.h
//  
//
//  Created by Maksim Golunov on 4/4/12.
//  Copyright (c) 2012. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const kMGPersistentCacheErrorDomain;

typedef enum {
    MGPersistentCacheErrorDownloadInProgress = 1
} MGPersistentCacheError;

@interface MGPersistentCache : NSObject

+ (instancetype)sharedCache;

+ (NSString*)localPathForURL:(NSURL*)url;

- (void)localPathForURL:(NSURL *)url completion:(void(^)(NSString* path, NSError* error))completion progress:(void(^)(NSUInteger bytesRead, long long totalBytes, long long totalBytesExpected))progress;

- (void)localPathsForURLs:(NSArray *)urls itemCompletion:(void (^)(NSURL *, NSString *, NSError *))itemCompletion itemProgress:(void (^)(NSURL*, NSUInteger, long long, long long))itemProgress overallCompletion:(void (^)(NSArray *, NSError *))overallCompletion;

+ (void)clearCacheForURL:(NSString *)remoteURL;
+ (void)clearCache;


@end
